/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

/**
 *
 * @author Isis
 */

import java.sql.*;
import java.sql.PreparedStatement;
import mysql.NovoConexaoBanco;

public class UsuarioDAO {
    private Connection con;
    
    public UsuarioDAO() throws SQLException{ 
        this.con = NovoConexaoBanco.abrir();
    } 
    
    /**
     *
     * @param usuario
     */
    public void add(Usuario usuario){ 
        String sql = "INSERT INTO usuario(tipo,login,senha) VALUES(?,?,?)";
        try {  
            try (PreparedStatement stmt = con.prepareStatement(sql)) {  
                stmt.setInt(1, usuario.getTipoUsuario());
                stmt.setString(2, usuario.getLoginUsuario());
                stmt.setString(3, usuario.getSenhaUsuario());
                stmt.execute();
            }  
            } catch (SQLException u) {  
                throw new RuntimeException(u);  
        }  
    }
    
    public boolean verificaLogin(Usuario usuario){
        String sql = "SELECT login, senha FROM usuario WHERE login=? and senha=?";
        try {  
            try (PreparedStatement stmt = con.prepareStatement(sql)) {  
                stmt.setString(1, usuario.getLoginUsuario());
                stmt.setString(2, usuario.getSenhaUsuario());
                ResultSet rs = stmt.executeQuery();
                if(rs.next()){
                    return true;
                }
                else{
                    stmt.close();
                    return false;
                }
            }  
            } catch (SQLException u) {  
                throw new RuntimeException(u);  
        }  
    }
    
    public int getTipoUsuario(Usuario usuario){
        String sql = "SELECT tipo FROM usuario WHERE login=? and senha=?";
        try {  
            try (PreparedStatement stmt = con.prepareStatement(sql)) {  
                stmt.setString(1, usuario.getLoginUsuario());
                stmt.setString(2, usuario.getSenhaUsuario());
                ResultSet rs = stmt.executeQuery();
                if(rs.next()){
                    int tipo = rs.getInt("tipo");
                    return tipo;
                }
                else{
                    stmt.close();
                    return 0;
                }
            }  
            } catch (SQLException u) {  
                throw new RuntimeException(u);  
        }  
    }
    
    public int getIdUsuario(Usuario usuario){
        String sql = "SELECT idusuario FROM usuario WHERE login=? and senha=?";
        try {  
            try (PreparedStatement stmt = con.prepareStatement(sql)) {  
                stmt.setString(1, usuario.getLoginUsuario());
                stmt.setString(2, usuario.getSenhaUsuario());
                ResultSet rs = stmt.executeQuery();
                if(rs.next()){
                    int id = rs.getInt("idusuario");
                    return id;
                }
                else{
                    stmt.close();
                    return 0;
                }
            }  
            } catch (SQLException u) {  
                throw new RuntimeException(u);  
        }  
    }
}
