/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

/**
 *
 * @author Isis
 */
public class Projeto {
    private int idProjeto;
    private String descricao;
    private String cliente;
    private String nome;
    private String gerente;
    
    public Projeto(String nome, String descricao, String cliente, String gerente){
        this.nome = nome;
        this.descricao = descricao;
        this.cliente = cliente;
        this.gerente = gerente;
        this.idProjeto = 0;
    }
    
    public void setIdProjeto(int idProjeto){
        this.idProjeto = idProjeto;
    }
    
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public void setCliente(String cliente){
        this.cliente = cliente;
    }
    
    public void setGerente(String gerente){
        this.gerente = gerente;
    }
    
    public int getIdProjeto(){
        return this.idProjeto;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public String getDescricao(){
        return this.descricao;
    }
    
    public String getCliente(){
        return this.cliente;
    }
    
    public String getGerente(){
        return this.gerente;
    }
}
