/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

/**
 *
 * @author Isis
 */
public class Requisito {
    private int idRequisito;
    private String descRequisito;
    private int tipoRequisito;
    private int idUsuario;
    private int idProjeto;
    
    public Requisito(int tipo, String descricao, int idUsuario, int idProjeto){
        this.tipoRequisito = tipo;
        this.descRequisito = descricao;
        this.idUsuario = idUsuario;
        this.idProjeto = idProjeto;
        this.idRequisito = 0;
    }
    
    public void setIdRequisito(int idReq){
        this.idRequisito = idReq;
    }
    
    public void setDescricao(String descricao){
        this.descRequisito = descricao;
    }
    
    public void setTipo(int tipo){
        this.tipoRequisito = tipo;
    }
    
    public void setIdUsuario(int idUsuario){
        this.idUsuario = idUsuario;
    }
    
    public void setIdProjeto(int idProjeto){
        this.idProjeto = idProjeto;
    }
    
    public int getIdRequisito(){
        return idRequisito;
    }
    
    public String getDescricao(){
        return descRequisito;
    }
    
    public int getTipo(){
        return tipoRequisito;
    }
    
    public int getIdUsuario(){
        return idUsuario;
    }
    
    public int getIdProjeto(){
        return idProjeto;
    }
}
