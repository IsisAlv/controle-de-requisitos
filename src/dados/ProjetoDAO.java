/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mysql.NovoConexaoBanco;
/**
 *
 * @author Isis
 */
public class ProjetoDAO {
    @SuppressWarnings("FieldMayBeFinal")
    private Connection con;
    
    public ProjetoDAO() throws SQLException{
        this.con = NovoConexaoBanco.abrir();
    }
    
    public void add(Projeto projeto){
        String sql = "INSERT INTO projetos(nome, descricao, cliente, gerente) VALUES(?,?,?,?)";
        try {  
            try (PreparedStatement stmt = con.prepareStatement(sql)) {  
                stmt.setString(1, projeto.getNome());
                stmt.setString(2, projeto.getDescricao());
                stmt.setString(3, projeto.getCliente());
                stmt.setString(4, projeto.getGerente());
                stmt.execute();
            }  
            } catch (SQLException u) {  
                throw new RuntimeException(u);  
        }
    }
}
