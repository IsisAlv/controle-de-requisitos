/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import mysql.NovoConexaoBanco;

/**
 *
 * @author Isis
 */
public class RequisitoDAO {
    private Connection con;
    
    public RequisitoDAO() throws SQLException{
        this.con = NovoConexaoBanco.abrir();
    }
    
    public void add(Requisito req){
        String sql = "INSERT INTO requisitos(tipo, descricao, idusuario, idprojeto) VALUES(?,?,?,?)";
        try {  
            try (PreparedStatement stmt = con.prepareStatement(sql)) {  
                stmt.setInt(1, req.getTipo());
                stmt.setString(2, req.getDescricao());
                stmt.setInt(3, req.getIdUsuario());
                stmt.setInt(4, req.getIdProjeto());
                stmt.execute();
            }  
            } catch (SQLException u) {  
                throw new RuntimeException(u);  
        }
    }
    
    public void deleteByUserType(int tipoReq, int tipoUsuario) throws SQLException{
        String sql = "SELECT idusuario FROM usuario WHERE tipo=?", sql2;
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, tipoUsuario);
        ResultSet rs = stmt.executeQuery();
        
        if(tipoReq != -1){
            sql2 = "DELETE FROM requisitos WHERE tipo=? and idusuario=?";
            while(rs.next()){
                PreparedStatement stmt2 = con.prepareStatement(sql2);
                stmt2.setInt(1, tipoReq);
                stmt2.setInt(2, rs.getInt("idusuario"));
                stmt2.execute();
            }
        }
        else{
            sql2 = "DELETE FROM requisitos WHERE idusuario=?";
            while(rs.next()){
                PreparedStatement stmt2 = con.prepareStatement(sql2);
                stmt2.setInt(1, rs.getInt("idusuario"));
                stmt2.execute();
            }
        }
        rs.close();
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteByUser(int tipoReq, String login) throws SQLException{
        String sql = "SELECT idusuario FROM usuario WHERE login=?", sql2;
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setString(1, login);
        ResultSet rs = stmt.executeQuery();
        
        if(tipoReq != -1){
            sql2 = "DELETE FROM requisitos WHERE tipo=? and idusuario=?";
            while(rs.next()){
                PreparedStatement stmt2 = con.prepareStatement(sql2);
                stmt2.setInt(1, tipoReq);
                stmt2.setInt(2, rs.getInt("idusuario"));
                stmt2.execute();
            }
        }
        else{
            sql2 = "DELETE FROM requisitos WHERE idusuario=?";
            while(rs.next()){
                PreparedStatement stmt2 = con.prepareStatement(sql2);
                stmt2.setInt(1, rs.getInt("idusuario"));
                stmt2.execute();
            }
        }
        rs.close();
    }
    
    public void deleteAll() throws SQLException{
        String sql = "DELETE FROM requisitos";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.execute();
    }
    
    public void deleteByReqID(int id) throws SQLException{
        String sql = "DELETE FROM requisitos WHERE idrequisito=?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.execute();
    }
    
    public void deleteByReqType(int tipo) throws SQLException{
        String sql = "DELETE FROM requisitos WHERE tipo=?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, tipo);
        stmt.execute();
    }
    
    public DefaultTableModel buscaUserType(int tipoReq, int tipoUsuario) throws SQLException{
        String sql;
        PreparedStatement stmt;
        ResultSet rs;
        if(tipoReq != -1){//Busca por tipo usuario x tipo requisito
            //Inner join exemplo
            sql = "SELECT requisitos.idrequisito, requisitos.tipo, requisitos.descricao, usuario.login, projetos.nome FROM requisitos INNER JOIN usuario ON requisitos.idusuario=usuario.idusuario INNER JOIN projetos ON requisitos.idprojeto=projetos.idprojeto WHERE requisitos.tipo=? and usuario.tipo=?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, tipoReq);
            stmt.setInt(2, tipoUsuario);
        }
        else{//Busca por tipo usuario apenas
            sql = "SELECT requisitos.idrequisito, requisitos.tipo, requisitos.descricao, usuario.login, projetos.nome FROM requisitos INNER JOIN usuario ON requisitos.idusuario=usuario.idusuario INNER JOIN projetos ON requisitos.idprojeto=projetos.idprojeto WHERE usuario.tipo=?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, tipoUsuario);
        }
        rs = stmt.executeQuery();
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID do Requisito");
        model.addColumn("Tipo de Requisito");
        model.addColumn("Descrição");
        model.addColumn("Usuário");
        model.addColumn("Projeto");
        while(rs.next()){
            String c2 = getTxtTipo(rs.getInt("requisitos.tipo"));
            model.addRow(new Object[]{rs.getInt("requisitos.idrequisito"),c2,rs.getString("requisitos.descricao"),rs.getString("usuario.login"),rs.getString("projetos.nome")});
        }
        
        return model;
    }
    
    public DefaultTableModel buscaUserName(int tipoReq, String login) throws SQLException{
        String sql;
        PreparedStatement stmt;
        ResultSet rs;
        if(tipoReq != -1){//Busca por nome usuario x tipo usuario
            sql = "SELECT requisitos.idrequisito, requisitos.tipo, requisitos.descricao, usuario.login, projetos.nome FROM requisitos INNER JOIN usuario ON requisitos.idusuario=usuario.idusuario INNER JOIN projetos ON requisitos.idprojeto=projetos.idprojeto WHERE requisitos.tipo=? and usuario.login=?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, tipoReq);
            stmt.setString(2, login);
        }
        else{//Busca por nome usuario apenas
            sql = "SELECT requisitos.idrequisito, requisitos.tipo, requisitos.descricao, usuario.login, projetos.nome FROM requisitos INNER JOIN usuario ON requisitos.idusuario=usuario.idusuario INNER JOIN projetos ON requisitos.idprojeto=projetos.idprojeto WHERE usuario.login=?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, login);
        }
        rs = stmt.executeQuery();
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID do Requisito");
        model.addColumn("Tipo de Requisito");
        model.addColumn("Descrição");
        model.addColumn("Usuário");
        model.addColumn("Projeto");
        while(rs.next()){
            String c2 = getTxtTipo(rs.getInt("requisitos.tipo"));
            model.addRow(new Object[]{rs.getInt("requisitos.idrequisito"),c2,rs.getString("requisitos.descricao"),rs.getString("usuario.login"), rs.getString("projetos.nome")});
        }
        
        return model;
    }
    
    public DefaultTableModel buscaReqType(int tipoReq) throws SQLException{
        String sql;
        PreparedStatement stmt;
        ResultSet rs;
        //Busca por tipo usuario apenas
        sql = "SELECT requisitos.idrequisito, requisitos.tipo, requisitos.descricao, usuario.login, projetos.nome FROM requisitos INNER JOIN usuario ON requisitos.idusuario=usuario.idusuario INNER JOIN projetos ON requisitos.idprojeto=projetos.idprojeto WHERE requisitos.tipo=?";
        stmt = con.prepareStatement(sql);
        stmt.setInt(1, tipoReq);
        
        rs = stmt.executeQuery();
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID do Requisito");
        model.addColumn("Tipo de Requisito");
        model.addColumn("Descrição");
        model.addColumn("Usuário");
        model.addColumn("Projeto");
        while(rs.next()){
            String c2 = getTxtTipo(rs.getInt("requisitos.tipo"));
            model.addRow(new Object[]{rs.getInt("requisitos.idrequisito"),c2,rs.getString("requisitos.descricao"),rs.getString("usuario.login"),rs.getString("projetos.nome")});
        }
        
        return model;
    }
    
    public DefaultTableModel buscaAll() throws SQLException{
        String sql;
        PreparedStatement stmt;
        ResultSet rs;
        //Busca por tipo usuario apenas
        sql = "SELECT requisitos.idrequisito, requisitos.tipo, requisitos.descricao, usuario.login, projetos.nome FROM requisitos INNER JOIN usuario ON requisitos.idusuario=usuario.idusuario INNER JOIN projetos ON requisitos.idprojeto=projetos.idprojeto";
        stmt = con.prepareStatement(sql);
        
        rs = stmt.executeQuery();
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID do Requisito");
        model.addColumn("Tipo de Requisito");
        model.addColumn("Descrição");
        model.addColumn("Usuário");
        model.addColumn("Projeto");
        while(rs.next()){
            String c2 = getTxtTipo(rs.getInt("requisitos.tipo"));
            model.addRow(new Object[]{rs.getInt("requisitos.idrequisito"),c2,rs.getString("requisitos.descricao"),rs.getString("usuario.login"),rs.getString("projetos.nome")});
        }
        
        return model;
    }
    
    private String getTxtTipo(int tipoReq){
        switch(tipoReq){
            case 0:
                return "RF";
            case 1:
                return "NF(U)";
            case 2:
                return "NF(C)";
            case 3:
                return "NF(D)";
            case 4:
                return "NF(S)";
            case 5:
                return "RP";
        }
        return "";
    }
}

