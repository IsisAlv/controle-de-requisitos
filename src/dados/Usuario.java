/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

/**
 *
 * @author Isis
 */
public class Usuario {
    
    private int idUsuario;
    private int tipoUsuario;
    private String loginUsuario;
    private String senhaUsuario;
    
    public Usuario(int tipo, String login, String senha){
        this.idUsuario = 0;
        this.tipoUsuario = tipo;
        this.loginUsuario = login;
        this.senhaUsuario = senha;
    }
    
    public Usuario(String login, String senha){
        this.loginUsuario = login;
        this.senhaUsuario = senha;
        this.idUsuario = 0;
        this.tipoUsuario = 0;
    }
    
    public int getIdUsuario(){
        return idUsuario;
    }
    
    public int getTipoUsuario(){
        return tipoUsuario;
    }
    
    public String getLoginUsuario(){
        return loginUsuario;
    }
    
    public String getSenhaUsuario(){
        return senhaUsuario;
    }
    
    public void  setIdUsuario(int id){
        this.idUsuario = id;
    }
    
    public void setTipoUsuario(int tipo){
        this.tipoUsuario = tipo;
    }
    
    public void setLoginUsuario(String login){
        this.loginUsuario = login;
    }
    
    public void setSenhaUsuario(String senha){
        this.senhaUsuario = senha;
    }
}
