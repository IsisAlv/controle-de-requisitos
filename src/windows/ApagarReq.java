/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import dados.RequisitoDAO;
import dados.Usuario;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Isis
 */
public class ApagarReq extends javax.swing.JFrame {

    /**
     * Creates new form ApagarReq
     */
    private Usuario user;
    
    public ApagarReq() {
        initComponents();
        user.setIdUsuario(0);
        user.setLoginUsuario("");
        user.setSenhaUsuario("");
        user.setTipoUsuario(0);
    }
    
    public ApagarReq(Usuario user) {
        initComponents();
        this.user = user;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnVoltar2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        cmbTipoUsuario = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbTipoReq = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        btnApagar1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnApagar2 = new javax.swing.JButton();
        txtID = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Apagar Requisito");
        setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        setLocation(new java.awt.Point(400, 200));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        btnVoltar2.setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        btnVoltar2.setText("Voltar");
        btnVoltar2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnVoltar2.setContentAreaFilled(false);
        btnVoltar2.setFocusPainted(false);
        btnVoltar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltar2ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cmbTipoUsuario.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        cmbTipoUsuario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "Desenvolvedor", "Cliente", "Gerente", "Usuário Final" }));
        cmbTipoUsuario.setBorder(null);
        cmbTipoUsuario.setFocusable(false);
        cmbTipoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoUsuarioActionPerformed(evt);
            }
        });

        jLabel2.setFont(getFont());
        jLabel2.setText("Grupo do usuário:");

        txtUsuario.setFont(getFont());
        txtUsuario.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });

        jLabel3.setFont(getFont());
        jLabel3.setText("Usuário:");

        cmbTipoReq.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        cmbTipoReq.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "Requisito de Software Funcional", "Requisito de Software Não Funcional (Usabilidade)", "Requisito de Software Não Funcional (Confiabilidade)", "Requisito de Software Não Funcional (Desempenho)", "Requisito de Software Não Funcional (Supportability)", "Restrições de Projeto" }));
        cmbTipoReq.setBorder(null);
        cmbTipoReq.setFocusable(false);

        jLabel4.setFont(getFont());
        jLabel4.setText("Tipo de Requisito:");

        btnApagar1.setFont(getFont());
        btnApagar1.setText("Apagar");
        btnApagar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnApagar1.setContentAreaFilled(false);
        btnApagar1.setFocusPainted(false);
        btnApagar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApagar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnApagar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTipoReq, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbTipoReq, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btnApagar1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(getFont());
        jLabel1.setText("Número de ID:");

        btnApagar2.setFont(getFont());
        btnApagar2.setText("Apagar");
        btnApagar2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnApagar2.setContentAreaFilled(false);
        btnApagar2.setFocusPainted(false);
        btnApagar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApagar2ActionPerformed(evt);
            }
        });

        txtID.setFont(getFont());
        txtID.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnApagar2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel1)
                    .addContainerGap(416, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnApagar2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel1)
                    .addContainerGap(43, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnVoltar2, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(btnVoltar2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVoltar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltar2ActionPerformed
        // Voltar
        MenuFunc func = new MenuFunc(user);
        func.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnVoltar2ActionPerformed

    @SuppressWarnings("null")
    private void btnApagar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApagar2ActionPerformed
        // Apagar Requisito 2 - Por Id do Requisito (um por vez)
        RequisitoDAO rdao = null;
        try {
            rdao = new RequisitoDAO();
        } catch (SQLException ex) {
            Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(txtID.getText().compareTo("")!=0){
            int r = JOptionPane.showConfirmDialog(rootPane, "Deseja realmente apagar este requisito?", "Apagar Requisito", JOptionPane.YES_NO_OPTION);
            if(r == JOptionPane.YES_OPTION){
                try {
                    rdao.deleteByReqID(Integer.parseInt(txtID.getText()));
                } catch (SQLException ex) {
                    Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(rootPane, "Requisito apagado com sucesso.", "Apagar Requisito", JOptionPane.PLAIN_MESSAGE);
                txtID.setText("");
            }
        }
        else{
            JOptionPane.showMessageDialog(rootPane, "Campo vazio! Se você deseja apagar todos os requisitos, utilize a opção acima.", "Apagar Requisito", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnApagar2ActionPerformed

    private void cmbTipoUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoUsuarioActionPerformed
        // Mudança do item selecionado
        if(cmbTipoUsuario.getSelectedIndex() == 0){
            txtUsuario.setEnabled(true);
            txtUsuario.requestFocus();
        }
        else {
            txtUsuario.setEnabled(false);
            txtUsuario.setText("");
        }
    }//GEN-LAST:event_cmbTipoUsuarioActionPerformed

    private void btnApagar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApagar1ActionPerformed
// Apagar Requisito 1
        RequisitoDAO rdao = null;
        try {
            rdao = new RequisitoDAO();
        } catch (SQLException ex) {
            Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
        }
        int r = JOptionPane.showConfirmDialog(rootPane, "Deseja realmente apagar este(s) requisito(s)?", "Apagar Requisitos", JOptionPane.YES_NO_OPTION);
        if(r == JOptionPane.YES_OPTION){
            if(cmbTipoUsuario.getSelectedIndex() != 0){
//Apagar por tipo de usuario
                try {
                    rdao.deleteByUserType(cmbTipoReq.getSelectedIndex()-1,cmbTipoUsuario.getSelectedIndex()-1);
                } catch (SQLException ex) {
                    Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
                if(txtUsuario.getText().compareTo("") != 0){
//Apagar por nome usuario
                    try {
                        rdao.deleteByUser(cmbTipoReq.getSelectedIndex()-1, txtUsuario.getText());
                    } catch (SQLException ex) {
                        Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else{
                    if(cmbTipoReq.getSelectedIndex() == 0){
//Apaga todos
                        try {
                        rdao.deleteAll();
                        } catch (SQLException ex) {
                            Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else{
//Apaga por tipo de requisito
                        try {
                        rdao.deleteByReqType(cmbTipoReq.getSelectedIndex()-1);
                        } catch (SQLException ex) {
                            Logger.getLogger(ApagarReq.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            JOptionPane.showMessageDialog(rootPane, "Requisito(s) apagado(s) com sucesso.", "Apagar Requisitos", JOptionPane.PLAIN_MESSAGE);
            txtUsuario.setText("");
            cmbTipoReq.setSelectedIndex(0);
            cmbTipoUsuario.setSelectedIndex(0);
        }
    }//GEN-LAST:event_btnApagar1ActionPerformed

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // texto titulo
        this.setTitle("Apagar Requisito - Usuário: "+user.getLoginUsuario());
        txtUsuario.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ApagarReq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ApagarReq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ApagarReq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ApagarReq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ApagarReq().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnApagar1;
    private javax.swing.JButton btnApagar2;
    private javax.swing.JButton btnVoltar2;
    private javax.swing.JComboBox<String> cmbTipoReq;
    private javax.swing.JComboBox<String> cmbTipoUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
