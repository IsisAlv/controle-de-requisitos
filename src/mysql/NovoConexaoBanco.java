/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysql;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Isis
 */
public class NovoConexaoBanco {
    
    public static String status = "Não conectou...";
    
    public NovoConexaoBanco(){}
    
    public static java.sql.Connection abrir() throws SQLException {
        Connection conn;
        conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/requisitos?useSSL=false", "root", "@_Isis280997");
        if (conn != null) {
 
                status = ("STATUS--->Conectado com sucesso!");
 
            } else {
 
                status = ("STATUS--->Não foi possivel realizar conexão");
 
            }
        return conn;
    }
    
    public static String statusConection() {
 
        return status;
 
    }
    
    public static boolean FecharConexao() {
 
        try {
 
            NovoConexaoBanco.abrir().close();
 
            return true;
 
        } catch (SQLException e) {
 
            return false;
 
        }
    }
    
    public static java.sql.Connection ReiniciarConexao() throws SQLException {
 
        FecharConexao();
 
  
 
        return NovoConexaoBanco.abrir();
 
    }
}
